package com.mercury.test;

import org.testng.annotations.Test;
import org.testng.annotations.Test;

import com.mercury.actionlib.BrowserAction;
import com.mercury.pageobject.LoginPage;
import com.mercury.util.ScreenShot;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class LoginTest {
	LoginPage login;

	 @BeforeMethod
	  public void before(){
		 login = PageFactory.initElements(BrowserAction.driver, LoginPage.class);
	  }
	
  @Test
  public void login(){
	  login.login(); 
	  String a = LoginPage.loginTextassert.getText();
	  assertEquals("SIGN-OFF", a);
  }
  @Test(dependsOnMethods={"login"})
  public void failure(){
	  Assert.fail("Failure case");
  }
  @AfterMethod
  public void screenshot(ITestResult result){
	  ScreenShot.Screen_Shot(result);
	  //changes
  }

}
