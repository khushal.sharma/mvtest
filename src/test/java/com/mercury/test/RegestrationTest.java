package com.mercury.test;


import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import org.testng.AssertJUnit;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import org.testng.annotations.Test;

import com.mercury.actionlib.BrowserAction;
import com.mercury.pageobject.LoginPage;
import com.mercury.pageobject.RegistrationPage;
import com.mercury.util.ScreenShot;
import com.mercury.util.VideoRec;

import atu.testrecorder.ATUTestRecorder;
import atu.testrecorder.exceptions.ATUTestRecorderException;
public class RegestrationTest   {
	//WebDriver driver;
	RegistrationPage regusr;
	LoginPage login;
	VideoRec rec = new VideoRec();
  @BeforeClass
  public void BrowserLaunch() throws ATUTestRecorderException {
	  BrowserAction.launchBrowser();
	 rec.videoRec();
	  
		
  
 
  }
  @BeforeMethod
  public void before(){
	  regusr = PageFactory.initElements(BrowserAction.driver, RegistrationPage.class);
	login = PageFactory.initElements(BrowserAction.driver, LoginPage.class);
  }
  @Test(priority=0)
  public void reg() throws Exception{
	  regusr.reg();
	  assertEquals("http://newtours.demoaut.com/create_account_success.php", BrowserAction.driver.getCurrentUrl());
	   
  }
  @Test(priority=1)
  public void login(){
	  login.login(); 
	  String a = LoginPage.loginTextassert.getText();
	  assertEquals("SIGN-OFF", a);
  }
  @Test(priority=2)
  public void failure(){
	  Assert.fail();
  }
  @AfterMethod
  public void screenshot(ITestResult result){
	  ScreenShot.Screen_Shot(result);
  }
  @AfterTest
  public void close() throws ATUTestRecorderException{
	  
	  BrowserAction.driver.quit();
	 
  }
}
