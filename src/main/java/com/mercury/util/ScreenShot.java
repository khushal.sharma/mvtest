package com.mercury.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;

import com.mercury.actionlib.BrowserAction;

public class ScreenShot {
	//final static WebDriver driver = null;
	public static void Screen_Shot(ITestResult result){
		if(!result.isSuccess()){
		 File src= ((TakesScreenshot)BrowserAction.driver).getScreenshotAs(OutputType.FILE);
		 try {
		  // now copy the  screenshot to desired location using copyFile //method
		 FileUtils.copyFile(src, new File("/home/khushal/git/Sun_Mercury/FailCase/"+System.currentTimeMillis()+".png"));	
		 }
		  
		 catch (IOException e)
		  {
		   System.out.println(e.getMessage());
		  
		  }

		}
		else{
			 File src= ((TakesScreenshot)BrowserAction.driver).getScreenshotAs(OutputType.FILE);
			 try {
			  // now copy the  screenshot to desired location using copyFile //method
			 FileUtils.copyFile(src, new File("/home/khushal/git/Sun_Mercury/PassCase/"+System.currentTimeMillis()+".png"));	
			 }
			  
			 catch (IOException e)
			  {
			   System.out.println(e.getMessage());
			  
			  }
		}
	
	}
	
}
