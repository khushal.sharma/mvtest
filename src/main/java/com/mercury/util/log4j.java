package com.mercury.util;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class log4j {
	public static Logger log; 
	public static void properties(){
		BasicConfigurator.configure();
		log = Logger.getLogger("log4j");
		PropertyConfigurator.configure("log4j.properties");
		//log.debug(message);
	}
	
	public static void browserLaunch(){
		log4j.properties();
		log.info("browser is launching and opening Mercury Website");
		
	}
	
	public static void reg(){
		log4j.properties();
		log.info("User registered successfully");
		
	}
}
