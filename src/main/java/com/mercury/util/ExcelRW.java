package com.mercury.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelRW {
private static XSSFSheet ExcelWSheet;
private static XSSFWorkbook ExcelWBook;
private static XSSFRow Row;
private static XSSFCell Cell;
static String filepath = "/home/khushal/Desktop/TestData.xlsx";
public static void setExcelfile() throws Exception{
	
	
	try {
		FileInputStream ExcelFile = new FileInputStream(filepath);
		
		ExcelWBook = new XSSFWorkbook(ExcelFile);
		ExcelWSheet = ExcelWBook.getSheetAt(0);
		
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
	
	
}

	
	public static String getCellData(int RowNum, int ColNum) throws Exception {
		setExcelfile();
		try {
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			
			String CellData = Cell.getStringCellValue();
			return CellData;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return"";
		}
		
		
	}
	
	
	
		public static void setCellData(String name, String password) throws Exception{
		setExcelfile();
		
		ExcelWSheet.createRow(0).createCell(0).setCellValue(name);
		ExcelWSheet.createRow(1).createCell(0).setCellValue(password);
		FileOutputStream fos = new FileOutputStream(filepath);
		ExcelWBook.write(fos);
		ExcelWBook.close();
		
	}
	
	
}
