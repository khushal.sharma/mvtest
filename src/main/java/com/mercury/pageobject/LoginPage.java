package com.mercury.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mercury.util.ExcelRW;


public class LoginPage {

	final WebDriver driver;
	
	public LoginPage(WebDriver driver){
		this.driver = driver;
	}
	@FindBy(xpath="//a[contains(text(),'sign-in')]")
	static
	WebElement signLink;
	
	@FindBy(name="userName")
	static WebElement userName;
	
	@FindBy(name="password")
	static WebElement password;
	
	@FindBy(name="login")
	static WebElement loginButton;
	
	// Assert xpath
	@FindBy(xpath="//a[contains(text(),'SIGN-OFF')]")
	public static WebElement loginTextassert;
	
	
	public static void login(){
		signLink.click();
		//log4j.log.info("Entering username and password");
		try {
			userName.sendKeys(ExcelRW.getCellData(0, 0));
			password.sendKeys(ExcelRW.getCellData(1, 0));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		loginButton.click();
		//log4j.log.info("Successfully logging");
		
		
	}
	
}
