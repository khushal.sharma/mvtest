package com.mercury.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.mercury.actionlib.DropDown;
import com.mercury.util.ExcelRW;
import com.mercury.util.log4j;


public class RegistrationPage {
		final WebDriver driver;
		
		public RegistrationPage(WebDriver driver){
			this.driver = driver;
		}
		
		@FindBy(xpath="//a[contains(text(),'REGISTER')]")
		public static WebElement register_button;
		
		@FindBy(name="firstName")
		public static WebElement first_name;
		@FindBy(name="lastName")
		public static WebElement last_name;
		@FindBy(name="phone")
		public static WebElement phone_number;
		@FindBy(name="userName")
		public  static WebElement email;
		@FindBy(name="address1")
		public static WebElement address1;
		@FindBy(name="address2")
		public static WebElement address2;
		@FindBy(name="city")
		public static WebElement city;
		@FindBy(name="state")
		public static WebElement state;
		
		@FindBy(name="postalCode")
		public static WebElement postal_code;
		@FindBy(name="country")
		public static WebElement country;
		@FindBy(name="email")
		public static WebElement user_name;
		@FindBy(name="password")
		public static WebElement password;
		@FindBy(name="confirmPassword")
		public static WebElement confirm_password;
		@FindBy(name="register")
		public static WebElement submit_button;
		
	
		
		
		
		
	public static void reg(){
		register_button.click();
		//log4j.log.info("Click on register button and filling all form");
		first_name.sendKeys("Khushal");
		last_name.sendKeys("Sharma");
		phone_number.sendKeys("99999999");
		email.sendKeys("khushal.sharma"+System.currentTimeMillis()+"@gmail.com");
		address1.sendKeys("chitrla circle"+Math.floor((Math.random()*10)+1));
		address2.sendKeys("Sitapura");
		city.sendKeys("Jaipur");
		state.sendKeys("Rajasthan");
		postal_code.sendKeys("302033");
		//for country selection
		DropDown.DropDown("INDIA");
		String usr = "Khushal.Sharma"+System.currentTimeMillis();
		String pass = "123456789";
		user_name.sendKeys(usr);
		password.sendKeys(pass);
		confirm_password.sendKeys(pass);
		//String[] credential = {usr,pass};
		try {
			ExcelRW.setCellData(usr,pass);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		submit_button.click();
		//log4j.log.info("Successfully submitted the form");
		log4j.reg();
	}
		
		
		
		
}
