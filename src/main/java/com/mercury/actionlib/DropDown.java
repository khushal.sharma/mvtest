package com.mercury.actionlib;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.mercury.pageobject.RegistrationPage;

public class DropDown {
	public static void DropDown(String str){
		Select sel = new Select(RegistrationPage.country);
		sel.selectByVisibleText(str);
	}
}
