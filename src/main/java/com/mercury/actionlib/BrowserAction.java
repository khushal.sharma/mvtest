package com.mercury.actionlib;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.mercury.util.log4j;



public class BrowserAction {
	public static WebDriver driver = null;
	
	public static void launchBrowser(){
		System.setProperty("webdriver.chrome.driver", "/home/khushal/workspace/Driver/chromedriver");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://newtours.demoaut.com/");
		log4j.browserLaunch();
		//checking git
	}
	
	
}
